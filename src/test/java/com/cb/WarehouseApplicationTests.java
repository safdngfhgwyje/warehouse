package com.cb;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cb.entity.Product;
import com.cb.entity.Role;
import com.cb.entity.User;
import com.cb.entity.UserRole;
import com.cb.service.ProductService;
import com.cb.service.RoleService;
import com.cb.service.UserRoleService;
import com.cb.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class WarehouseApplicationTests {

    @Autowired
    private UserService userService;
    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private ProductService productService;

    @Test
    void contextLoads() {
        Page<User> userPage = new Page<>(2,2);
        userService.getUser(userPage,new User());
        System.out.println(userPage.getRecords());
    }

    @Test
    public void test01(){
        List<UserRole> byUserId = userRoleService.getByUserId(35);
        byUserId.forEach(System.out::println);
    }

    @Test
    public void test02(){
        List<Role> roleList = roleService.queryRolesByUserId(35);
        roleList.forEach(System.out::println);
    }

    @Test
    public void test03(){
        int i = userRoleService.delRoleByUserId(35);
        System.out.println(i);
    }

    @Test
    public void test04(){
        Page<Product> page = new Page<>(1 ,5);
        productService.queryProductPage(page, new Product());
        System.out.println(page.getRecords());
    }

}
