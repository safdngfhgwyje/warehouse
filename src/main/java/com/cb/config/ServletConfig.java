package com.cb.config;

import com.cb.filter.LoginCheckFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * Servlet的配置类
 */
@Configuration
public class ServletConfig {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Bean
    public FilterRegistrationBean filterRegistrationBean(){
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        //创建LoginCheckFilter对象
        LoginCheckFilter loginCheckFilter = new LoginCheckFilter();
        //注入redis模板
        loginCheckFilter.setStringRedisTemplate(stringRedisTemplate);
        //注册LoginCheckFilter
        filterRegistrationBean.setFilter(loginCheckFilter);
        //配置LoginCheckFilter拦截所有请求
        filterRegistrationBean.addUrlPatterns("/*");
        return filterRegistrationBean;
    }
}
