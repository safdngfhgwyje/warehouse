package com.cb.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 产地
 */
@TableName(value ="place")
@Data
public class Place implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer placeId;//产地id

    private String placeName;//产地名称

    private String placeNum;//产地代码

    private String introduce;//产地介绍

    private String isDelete;//是否删除状态,0未删除,1删除

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

}