package com.cb.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * 商品分类表
 */
@TableName(value ="product_type")
@Data
public class ProductType implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer typeId; //分类id

    private Integer parentId; //上级分类id

    private String typeCode; //分类代码

    private String typeName; //分类名称

    private String typeDesc; //分类描述

    //自定义List<ProductType>集合属性,用于存储当前分类的所有子级分类
    @TableField(exist = false)
    private List<ProductType> childProductCategory;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

}