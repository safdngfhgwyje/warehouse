package com.cb.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 品牌b表
 */
@TableName(value ="brand")
@Data
public class Brand implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer brandId; //品牌id

    private String brandName; //品牌名称

    private String brandLeter; //品牌首字母

    private String brandDesc; //品牌描述

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}