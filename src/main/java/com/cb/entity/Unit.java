package com.cb.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 规格单位表
 */
@TableName(value ="unit")
@Data
public class Unit implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer unitId;//单位id

    private String unitName;//单位

    private String unitDesc;//单位描述

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}