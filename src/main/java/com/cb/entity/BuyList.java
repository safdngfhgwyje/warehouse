package com.cb.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 采购单
 */
@TableName(value ="buy_list")
@Data
public class BuyList implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer buyId; //采购单id

    private Integer productId;//采购单采购的商品id

    private Integer storeId;//采购单采购的商品所在仓库id

    private Integer buyNum;//预计采购的商品数量

    private Integer factBuyNum;//实际采购的商品数量

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date buyTime;//采购时间

    private Integer supplyId;//采购单采购的商品的供应商id

    private Integer placeId;//采购单采购的商品的产地id

    private String buyUser;//采购人

    private String phone;//采购人联系电话

    private String isIn;//是否生成入库单,1.是,0.否

    //---------------追加属性---------------------------
    @TableField(exist = false)
    private String productName;//商品名称
    @TableField(exist = false)
    private String storeName;//仓库名称
    @TableField(exist = false)
    private String startTime;//搜索起始时间
    @TableField(exist = false)
    private String endTime;//搜索结束时间

}