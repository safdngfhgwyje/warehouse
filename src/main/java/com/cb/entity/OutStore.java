package com.cb.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 出库单
 */
@TableName(value ="out_store")
@Data
public class OutStore implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer outsId; //出库单id

    private Integer productId;//出库单出库的商品id

    private Integer storeId;//出库单出库的商品所在仓库id

    private Integer tallyId;//理货员id

    private Double outPrice;//商品的出库价格

    private Integer outNum;//出库的商品数量

    private Integer createBy;//创建出库单的用户id

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;//创建出库单的时间

    private String isOut;//是否出库,0.未出库,1.已出库

    @TableField(exist = false)
    private Double salePrice;
    //------------------追加的属性-------------------------
    @TableField(exist = false)
    private String productName;//商品名称
    @TableField(exist = false)
    private String startTime;//起始时间
    @TableField(exist = false)
    private String endTime;//结束时间
    @TableField(exist = false)
    private String storeName;//仓库名称
    @TableField(exist = false)
    private String userCode;//创建出库单的用户的名称
}