package com.cb.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 仓库表
 */
@TableName(value ="store")
@Data
public class Store implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer storeId;  //仓库id

    private String storeName;  //仓库名称

    private String storeNum;  //仓库编码

    private String storeAddress;  //仓库地址

    private String concat;  //仓库联系人

    private String phone;  //仓库联系电话

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}