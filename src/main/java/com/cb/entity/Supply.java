package com.cb.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 供货商
 */
@TableName(value ="supply")
@Data
public class Supply implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer supplyId; //供应商id

    private String supplyNum;//供应商代码

    private String supplyName;//供应商名称

    private String supplyIntroduce;//供应商介绍

    private String concat;//供应商联系人

    private String phone;//供应商联系电话

    private String address;//供应商地址

    private String isDelete;//是否删除状态,0未删除,1删除

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}