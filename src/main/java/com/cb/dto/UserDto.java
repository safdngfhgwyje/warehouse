package com.cb.dto;

import com.cb.entity.User;
import lombok.Data;

@Data
public class UserDto extends User {
    //创建人名字
    private String createByName;
}
