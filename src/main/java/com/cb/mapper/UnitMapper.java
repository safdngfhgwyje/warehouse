package com.cb.mapper;

import com.cb.entity.Unit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.cb.entity.Unit
 */
public interface UnitMapper extends BaseMapper<Unit> {

}




