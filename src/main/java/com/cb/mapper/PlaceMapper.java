package com.cb.mapper;

import com.cb.entity.Place;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.cb.entity.Place
 */
public interface PlaceMapper extends BaseMapper<Place> {

}




