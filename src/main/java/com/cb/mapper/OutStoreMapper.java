package com.cb.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cb.entity.OutStore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

public interface OutStoreMapper extends BaseMapper<OutStore> {

    //分页查询出库单
    IPage<OutStore> outStorePage(@Param("outStorePage") Page<OutStore> outStorePage, @Param("outStore") OutStore outStore);
}




