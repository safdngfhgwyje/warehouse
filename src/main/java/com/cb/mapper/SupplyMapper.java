package com.cb.mapper;

import com.cb.entity.Supply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.cb.entity.Supply
 */
public interface SupplyMapper extends BaseMapper<Supply> {

}




