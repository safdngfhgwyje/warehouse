package com.cb.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cb.entity.BuyList;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

public interface BuyListMapper extends BaseMapper<BuyList> {
    //分页查询采购单
    IPage<BuyList> selectPurchasePage(@Param("page") Page page, @Param("purchase") BuyList purchase);

}




