package com.cb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cb.entity.Role;

public interface RoleMapper extends BaseMapper<Role> {
}
