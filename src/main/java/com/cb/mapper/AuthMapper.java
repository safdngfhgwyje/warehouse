package com.cb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cb.entity.Auth;

import java.util.List;

public interface AuthMapper extends BaseMapper<Auth> {

    //根据用户id查询用户所有权限(菜单)的方法
    List<Auth> findAllAuth(int userId);

}
