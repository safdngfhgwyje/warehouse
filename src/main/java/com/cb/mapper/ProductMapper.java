package com.cb.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.cb.entity.Product;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

public interface ProductMapper extends BaseMapper<Product> {

    //分页查询商品
    IPage<Product> selectProductPage(@Param("page") IPage<?> page, @Param("product") Product product);
}




