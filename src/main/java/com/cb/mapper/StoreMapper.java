package com.cb.mapper;

import com.cb.entity.Statistics;
import com.cb.entity.Store;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

public interface StoreMapper extends BaseMapper<Store> {
    //统计各个仓库商品库存数量的方法
    List<Statistics> statisticsStoreInvent();
}




