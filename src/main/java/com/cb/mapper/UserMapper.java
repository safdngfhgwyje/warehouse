package com.cb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cb.entity.User;

public interface UserMapper extends BaseMapper<User> {
}
