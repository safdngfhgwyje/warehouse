package com.cb.mapper;

import com.cb.entity.ProductType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.cb.entity.ProductType
 */
public interface ProductTypeMapper extends BaseMapper<ProductType> {

}




