package com.cb.mapper;

import com.cb.entity.Brand;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.cb.entity.Brand
 */
public interface BrandMapper extends BaseMapper<Brand> {

}




