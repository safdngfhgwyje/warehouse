package com.cb.mapper;

import com.cb.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.cb.entity.UserRole
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}




