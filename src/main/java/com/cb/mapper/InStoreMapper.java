package com.cb.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cb.entity.InStore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

public interface InStoreMapper extends BaseMapper<InStore> {
    //分页查询入库单的方法
    IPage<InStore> selectInStorePage(@Param("page") Page page, @Param("inStore") InStore inStore);
}




