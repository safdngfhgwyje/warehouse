package com.cb.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cb.entity.*;
import com.cb.service.*;
import com.cb.utils.GiteeImgBedUtils;
import com.cb.utils.Result;
import com.cb.utils.TokenUtils;
import com.cb.utils.WarehouseConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private StoreService storeService;
    @Autowired
    private BrandService brandService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ProductTypeService productTypeServicel;
    @Autowired
    private SupplyService supplyService;
    @Autowired
    private PlaceService placeService;
    @Autowired
    private UnitService unitService;
    @Autowired
    private TokenUtils tokenUtils;

    /**
     *  查询所有仓库的url接口/product/store-list
     */
    @GetMapping("/store-list")
    public Result storeList(){
        List<Store> list = storeService.queryAllStore();
        return Result.ok(list);
    }

    /**
     * 查询所有品牌的url接口/product/brand-list
     */
    @GetMapping("/brand-list")
    public Result brandList(){
        List<Brand> brandList = brandService.queryAllBrand();
        return Result.ok(brandList);
    }

    /**
     * 分页查询商品的url接口/product/product-page-list
     */
    @GetMapping("/product-page-list")
    public Result productPageList(Product product,int pageNum,int pageSize){
        Page<Product> productPage = productList(product, pageNum, pageSize);
        return Result.ok(productPage);
    }

    public Page<Product> productList(Product product,int pageNum,int pageSize){
        Page<Product> productPage = new Page<>(pageNum, pageSize);
        productService.queryProductPage(productPage,product);
        List<Product> records = productPage.getRecords();
        records=records.stream().map((item)->{
            String imgs = item.getImgs();
            item.setImgs(GiteeImgBedUtils.getFile(imgs));
            return item;
        }).collect(Collectors.toList());
        return productPage;
    }

    /**
     * 导出数据的url接口/product/exportTable
     */
    @GetMapping("/exportTable")
    public Result exportTable(Product product,int pageNum,int pageSize){
        Page<Product> productPage = productList(product, pageNum, pageSize);
        return Result.ok(productPage.getRecords());
    }

    /**
     * 查询所有商品分类树的url接口/product/category-tree
     */
    @GetMapping("/category-tree")
    public Result categoryTree(){
        List<ProductType> typeTreeList = productTypeServicel.allProductTypeTree();
        return Result.ok(typeTreeList);
    }

    /**
     * 查询所有供应商的url接口/product/supply-list
     */
    @GetMapping("/supply-list")
    public Result supplyList(){
        List<Supply> supplyList=supplyService.queryAllSupply();
        return Result.ok(supplyList);
    }

    /**
     * 查询所有产地的url接口/product/place-list
     */
    @GetMapping("/place-list")
    public Result placeList(){
        List<Place> placeList = placeService.queryAllPlace();
        return Result.ok(placeList);
    }

    /**
     * 查询所有单位的url接口/product/unit-list
     */
    @GetMapping("/unit-list")
    public Result unitList(){
        List<Unit> unitList = unitService.queryAllUnit();
        return Result.ok(unitList);
    }

    /**
     * 上传图片的url接口/product/img-upload
     */
    @PostMapping("/img-upload")
    public Result uploadImg(@RequestParam("file") MultipartFile file, String imageName) throws IOException {
        System.out.println("file = " + file + ", imageName = " + imageName);
        return GiteeImgBedUtils.upload(file,imageName);
    }

    /**
     * 添加商品的url接口/product/product-add
     */
    @PostMapping("/product-add")
    public Result addProduct(@RequestBody Product product,
                             @RequestHeader(WarehouseConstants.HEADER_TOKEN_NAME) String token){
        //获取当前登录的用户id,即添加商品的用户id
        int createBy = tokenUtils.getId(token);
        product.setCreateBy(createBy);
        return productService.saveProduct(product);
    }

    /**
     * 修改商品上下架状态的url接口/product/state-change
     */
    @PutMapping("/state-change")
    public Result changeProductState(@RequestBody Product product){
        return  productService.updateProductState(product);
    }

    /**
     * 删除商品的url接口/product/product-delete/{productId}
     */
    @DeleteMapping("/product-delete/{productId}")
    public Result deleteProduct(@PathVariable Integer productId){
        return productService.deleteById(productId);
    }

    /**
     * 批量删除商品的url接口/product/product-list-delete
     */
    @DeleteMapping("/product-list-delete")
    public Result deleteProducts(@RequestBody List<Integer> ids){
        return productService.deleteBatch(ids);
    }

    /**
     * 修改商品的url接口/product/product-update
     */
    @PutMapping("/product-update")
    public Result updateProduct(@RequestBody Product product,
                                @RequestHeader(WarehouseConstants.HEADER_TOKEN_NAME) String token){
        //获取当前登录的用户id,即修改商品的用户id
        int updateBy = tokenUtils.getId(token);
        product.setUpdateBy(updateBy);
        //图片名
        String imgs = product.getImgs();
        String extractedString = imgs.substring(imgs.lastIndexOf("//test//") + "//test//".length());
        product.setImgs(extractedString);
        return productService.updateProduct(product);
    }

}
