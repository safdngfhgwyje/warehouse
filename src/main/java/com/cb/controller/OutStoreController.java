package com.cb.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cb.entity.OutStore;
import com.cb.entity.Store;
import com.cb.service.OutStoreService;
import com.cb.service.StoreService;
import com.cb.utils.Result;
import com.cb.utils.TokenUtils;
import com.cb.utils.WarehouseConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/outstore")
public class OutStoreController {
    @Autowired
    private OutStoreService outStoreService;
    @Autowired
    private TokenUtils tokenUtils;
    @Autowired
    private StoreService storeService;

    /**
     * 添加出库单的url接口/outstore/outstore-add
     */
    @PostMapping("/outstore-add")
    public Result addOutStore(@RequestBody OutStore outStore,
                              @RequestHeader(WarehouseConstants.HEADER_TOKEN_NAME) String token){
        int createBy = tokenUtils.getId(token);
        outStore.setCreateBy(createBy);
        return outStoreService.saveOutStore(outStore);
    }

    /**
     * 查询所有仓库的url接口/outstore/store-list
     */
    @GetMapping("/store-list")
    public Result storeList(){
        List<Store> stores = storeService.queryAllStore();
        return Result.ok(stores);
    }

    /**
     * 分页查询出库单的url接口/outstore/outstore-page-list
     */
    @GetMapping("/outstore-page-list")
    public Result outStorePageList(OutStore outStore,int pageNum,int pageSize){
        Page<OutStore> outStorePage = new Page<>(pageNum, pageSize);
        outStoreService.outStorePage(outStorePage,outStore);
        return Result.ok(outStorePage);
    }

    /**
     * 导出数据的url接口/outstore/exportTable
     */
    @GetMapping("/exportTable")
    public Result exportTable(OutStore outStore,int pageNum,int pageSize){
        Page<OutStore> outStorePage = new Page<>(pageNum, pageSize);
        outStoreService.outStorePage(outStorePage,outStore);
        return Result.ok(outStorePage.getRecords());
    }

    /**
     * 确定出库的url接口/outstore/outstore-confirm
     */
    @PutMapping("/outstore-confirm")
    public Result confirmOutStore(@RequestBody OutStore outStore){
        return outStoreService.confirmOutStore(outStore);
    }
}
