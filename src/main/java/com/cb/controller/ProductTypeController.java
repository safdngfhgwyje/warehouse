package com.cb.controller;

import com.cb.entity.ProductType;
import com.cb.service.ProductTypeService;
import com.cb.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/productCategory")
public class ProductTypeController {
    @Autowired
    private ProductTypeService productTypeService;

    /**
     * 查询商品分类树的url接口/productCategory/product-category-tree
     */
    @GetMapping("/product-category-tree")
    public Result productCategoryTree(){
        List<ProductType> productTypes = productTypeService.allProductTypeTree();
        return Result.ok(productTypes);
    }

    /**
     * 校验分类编码是否已存在的url接口/productCategory/verify-type-code
     */
    @GetMapping("/verify-type-code")
    public Result checkTypeCode(String typeCode){
        return productTypeService.queryTypeByCode(typeCode);
    }

    /**
     * 添加商品分类的url接口/productCategory/type-add
     */
    @PostMapping("/type-add")
    public Result addProductType(@RequestBody ProductType productType){
        return productTypeService.saveProductType(productType);
    }

    /**
     * 删除商品分类的url接口/productCategory/type-delete/{typeId}
     */
    @DeleteMapping("/type-delete/{typeId}")
    public Result deleteType(@PathVariable Integer typeId){
        return productTypeService.removeProductType(typeId);
    }

    /**
     * 修改商品分类的url接口/productCategory/type-update
     */
    @PutMapping("/type-update")
    public Result updateType(@RequestBody ProductType productType){
        return productTypeService.updateProductType(productType);
    }
}
