package com.cb.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cb.dto.AssignAuthDto;
import com.cb.entity.Auth;
import com.cb.entity.Role;
import com.cb.entity.User;
import com.cb.service.AuthService;
import com.cb.service.RoleService;
import com.cb.service.UserService;
import com.cb.utils.Result;
import com.cb.utils.TokenUtils;
import com.cb.utils.WarehouseConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/role")
public class RoleController {
    @Autowired
    private TokenUtils tokenUtils;
    @Autowired
    @Lazy
    private RoleService roleService;
    @Autowired
    private AuthService authService;
    @Autowired
    private UserService userService;

    /**
     * 查询所有角色的url接口role/role-list
     */
    @GetMapping("/role-list")
    public Result queryAllRole(){
        List<Role> roles = roleService.queryAllRole();
        return Result.ok(roles);
    }

    /**
     * 分页查询角色的url接口/role/role-page-list
     */
    @GetMapping("/role-page-list")
    public Result roleListPage(Role role,int pageNum,int pageSize){
        Page<Role> rolePage = roleList(role, pageNum, pageSize);
        return Result.ok(rolePage);
    }

    public Page<Role> roleList(Role role,int pageNum,int pageSize){
        Page<Role> rolePage = new Page<>(pageNum, pageSize);
        roleService.getRole(rolePage,role);
        List<Role> roleList = rolePage.getRecords();
        roleList=roleList.stream ().map((item)->{
            User user = userService.getById(item.getCreateBy());
            item.setGetCode(user.getUserCode());
            return item;
        }).collect(Collectors.toList());
        return rolePage;
    }

    /**
     * 导出数据的url接口/role/exportTable
     */
    @GetMapping("/exportTable")
    public Result exportTable(Role role,int pageNum,int pageSize){
        Page<Role> rolePage = roleList(role, pageNum, pageSize);
        return Result.ok(rolePage.getRecords());
    }

    /**
     * 添加角色的url接口/role/role-add
     */
    @PostMapping("/role-add")
    public Result addRole(@RequestBody Role role,
                          @RequestHeader(WarehouseConstants.HEADER_TOKEN_NAME) String token){
        int createBy = tokenUtils.getId(token);
        role.setCreateBy(createBy);
        return roleService.saveRole(role);
    }

    /**
     * 修改角色状态的url接口/role/role-state-update
     */
    @PutMapping("/role-state-update")
    public Result updateRoleState(@RequestBody Role role,
                                  @RequestHeader(WarehouseConstants.HEADER_TOKEN_NAME) String token){
        int updateBy = tokenUtils.getId(token);
        role.setUpdateBy(updateBy);
        role.setUpdateTime(new Date());
        boolean result = roleService.updateById(role);
        if(result){
            return Result.ok("修改成功！");
        }else {
            return Result.err(Result.CODE_ERR_BUSINESS, "修改失败！");
        }
    }

    /**
     *  查询角色已分配的权限(菜单)的url接口/role/role-auth
     */
    @GetMapping("/role-auth")
    public Result queryRoleAuth(Integer roleId){
        List<Auth> authList=authService.queryRoleAuth(roleId);
        ArrayList<Integer> integers = new ArrayList<>();
        for (Auth auth : authList) {
            integers.add(auth.getAuthId());
        }
        return Result.ok(integers);
    }

    /**
     * 给角色分配权限(菜单)的url接口/role/auth-grant
     */
    @PutMapping("/auth-grant")
    public Result assignAuth(@RequestBody AssignAuthDto assignAuthDto){
        authService.assignAuth(assignAuthDto);
        return Result.ok("分配权限成功！");
    }

    /**
     * 删除角色的url接口/role/role-delete/{roleId}
     */
    @DeleteMapping("/role-delete/{roleId}")
    public Result deleteRole(@PathVariable Integer roleId){
        roleService.deleteRole(roleId);
        return Result.ok("角色删除成功！");
    }

    /**
     * 修改角色的url接口/role/role-update
     */
    @PutMapping("/role-update")
    public Result updateRole(@RequestBody Role role,
                             @RequestHeader(WarehouseConstants.HEADER_TOKEN_NAME) String token){
        int createBy = tokenUtils.getId(token);
        role.setUpdateBy(createBy);
        return roleService.updateRole(role);
    }


}
