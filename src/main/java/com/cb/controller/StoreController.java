package com.cb.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cb.entity.Store;
import com.cb.service.StoreService;
import com.cb.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/store")
public class StoreController {
    @Autowired
    private StoreService storeService;

    /**
     * 分页查询仓库的url接口/store/store-page-list
     */
    @GetMapping("/store-page-list")
    public Result storePageList(Store store,int pageNum,int pageSize){
        Page<Store> storePage = new Page<>(pageNum, pageSize);
        storeService.queryStorePage(storePage,store);
        return Result.ok(storePage);
    }

    /**
     * 校验仓库编号的url接口/store/store-num-check
     */
    @GetMapping("/store-num-check")
    public Result checkStoreNum(String storeNum){
        return storeService.checkStoreNum(storeNum);
    }

    /**
     * 添加仓库的url接口/store/store-add
     */
    @PostMapping("/store-add")
    public Result addStore(@RequestBody Store store){
        boolean save = storeService.save(store);
        if(save){
            return Result.ok("仓库添加成功！");
        }
        return Result.err(Result.CODE_ERR_BUSINESS, "仓库添加失败！");
    }

    /**
     * 修改仓库的url接口/store/store-update
     */
    @PutMapping("/store-update")
    public Result updateStore(@RequestBody Store store){
        boolean b = storeService.updateById(store);
        if(b){
            return Result.ok("仓库修改成功！");
        }
        return Result.err(Result.CODE_ERR_BUSINESS, "仓库修改失败！");
    }

    /**
     * 删除仓库的url接口/store/store-delete/{storeId}
     */
    @DeleteMapping("/store-delete/{storeId}")
    public Result deleteStore(@PathVariable Integer storeId){
        boolean b = storeService.removeById(storeId);
        if(b){
            return Result.ok("仓库删除成功！");
        }
        return Result.err(Result.CODE_ERR_BUSINESS, "仓库删除失败！");
    }

    /**
     * 导出数据的url接口/store/exportTable
     */
    @GetMapping("/exportTable")
    public Result exportTable(Store store,int pageNum,int pageSize){
        Page<Store> storePage = new Page<>(pageNum, pageSize);
        storeService.queryStorePage(storePage,store);
        return Result.ok(storePage.getRecords());
    }
}
