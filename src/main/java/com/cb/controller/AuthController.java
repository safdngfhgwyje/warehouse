package com.cb.controller;

import com.cb.entity.Auth;
import com.cb.service.AuthService;
import com.cb.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/auth")
@RestController
public class AuthController {
    @Autowired
    private AuthService authService;

    /**
     * 查询整个权限(菜单)树的url接口/auth/auth-tree
     */
    @GetMapping("/auth-tree")
    public Result allAuthTree(){
        List<Auth> allAuthTree = authService.allAuthTree();
        return Result.ok(allAuthTree);
    }

}
