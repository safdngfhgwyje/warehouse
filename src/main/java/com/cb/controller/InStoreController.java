package com.cb.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cb.entity.InStore;
import com.cb.entity.Store;
import com.cb.service.InStoreService;
import com.cb.service.StoreService;
import com.cb.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/instore")
public class InStoreController {
    @Autowired
    private StoreService storeService;
    @Autowired
    private InStoreService inStoreService;

    /**
     * 查询所有仓库的url接口/instore/store-list
     */
    @GetMapping("/store-list")
    public Result storeList(){
        List<Store> stores = storeService.queryAllStore();
        return Result.ok(stores);
    }

    /**
     * 分页查询入库单的url接口/instore/instore-page-list
     */
    @GetMapping("/instore-page-list")
    public Result inStorePageList(InStore inStore,int pageNum,int pageSize){
        Page<InStore> inStorePage = new Page<>(pageNum, pageSize);
        inStoreService.queryInStorePage(inStorePage,inStore);
        return Result.ok(inStorePage);
    }

    /**
     * 导出数据的url接口/instore/exportTable
     */
    @GetMapping("/exportTable")
    public Result exportTable(InStore inStore,int pageNum,int pageSize){
        Page<InStore> inStorePage = new Page<>(pageNum, pageSize);
        inStoreService.queryInStorePage(inStorePage,inStore);
        return Result.ok(inStorePage.getRecords());
    }

    /**
     * 确定入库的url接口/instore/instore-confirm
     */
    @PutMapping("/instore-confirm")
    public Result confirmInStore(@RequestBody InStore inStore){
        return inStoreService.confirmInStore(inStore);
    }
}
