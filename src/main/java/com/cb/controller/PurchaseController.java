package com.cb.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cb.entity.BuyList;
import com.cb.entity.InStore;
import com.cb.entity.Store;
import com.cb.service.BuyListService;
import com.cb.service.InStoreService;
import com.cb.service.StoreService;
import com.cb.utils.Result;
import com.cb.utils.TokenUtils;
import com.cb.utils.WarehouseConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/purchase")
public class PurchaseController {
    @Autowired
    private BuyListService buyListService;
    @Autowired
    private StoreService storeService;
    @Autowired
    private TokenUtils tokenUtils;
    @Autowired
    private InStoreService inStoreService;

    /**
     * 添加采购单的url接口/purchase/purchase-add
     */
    @PostMapping("/purchase-add")
    public Result addPurchase(@RequestBody BuyList buyList){
        return buyListService.savePurchase(buyList);
    }

    /**
     * 查询所有仓库的url接口/purchase/store-list
     */
    @RequestMapping("/store-list")
    public Result storeList(){
        List<Store> storeList = storeService.queryAllStore();
        return Result.ok(storeList);
    }

    /**
     * 分页查询采购单的url接口/purchase/purchase-page-list
     */
    @GetMapping("/purchase-page-list")
    public Result purchasePageList(BuyList buyList,int pageNum,int pageSize){
        Page<BuyList> buyListPage = new Page<>(pageNum, pageSize);
        buyListService.queryPurchasePage(buyListPage,buyList);
        return Result.ok(buyListPage);
    }

    /**
     * 导出数据的url接口/purchase/exportTable
     */
    @GetMapping("/exportTable")
    public Result exportTable(BuyList buyList,int pageNum,int pageSize){
        Page<BuyList> buyListPage = new Page<>(pageNum, pageSize);
        buyListService.queryPurchasePage(buyListPage,buyList);
        return Result.ok(buyListPage.getRecords());
    }

    /**
     * 删除采购单的url接口/purchase/purchase-delete/{buyId}
     */
    @DeleteMapping("/purchase-delete/{buyId}")
    public Result deletePurchase(@PathVariable Integer buyId){
        return buyListService.deletePurchase(buyId);
    }

    /**
     * 的url接口/purchase/purchase-update
     */
    @PutMapping("/purchase-update")
    public Result updatePurchase(@RequestBody BuyList purchase){
        return buyListService.updatePurchase(purchase);
    }

    /**
     * 添加入库单的url接口/purchase/in-warehouse-record-add
     */
    @PostMapping("/in-warehouse-record-add")
    public Result addInStore(@RequestBody BuyList purchase,
                             @RequestHeader(WarehouseConstants.HEADER_TOKEN_NAME) String token){
        int createBy = tokenUtils.getId(token);
        InStore inStore = new InStore();
        inStore.setStoreId(purchase.getStoreId());
        inStore.setProductId(purchase.getProductId());
        inStore.setInNum(purchase.getFactBuyNum());
        inStore.setCreateBy(createBy);
        inStore.setCreateTime(new Date());
        inStore.setIsIn(0);
        return inStoreService.saveInStore(inStore,purchase.getBuyId());
    }

}
