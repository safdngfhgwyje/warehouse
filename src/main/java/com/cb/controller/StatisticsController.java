package com.cb.controller;

import com.cb.entity.Statistics;
import com.cb.service.StoreService;
import com.cb.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/statistics")
public class StatisticsController {
    @Autowired
    private StoreService storeService;

    /**
     * 统计各个仓库商品库存数量的url接口/statistics/store-invent
     */
    @GetMapping("/store-invent")
    public Result statisticsStoreInvent(){
        List<Statistics> statistics = storeService.statisticsStoreInvent();
        return Result.ok(statistics);
    }
}
