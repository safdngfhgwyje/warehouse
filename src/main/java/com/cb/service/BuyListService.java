package com.cb.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cb.entity.BuyList;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cb.utils.Result;

public interface BuyListService extends IService<BuyList> {

    //添加采购单
    Result savePurchase(BuyList buyList);

    //分页查询采购单
    IPage<BuyList> queryPurchasePage(Page<BuyList> buyListPage, BuyList buyList);

    //删除采购单
    Result deletePurchase(Integer buyId);

    //修改采购单
    Result updatePurchase(BuyList purchase);
}
