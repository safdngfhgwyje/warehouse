package com.cb.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cb.entity.OutStore;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cb.utils.Result;

public interface OutStoreService extends IService<OutStore> {

    //添加出库单
    Result saveOutStore(OutStore outStore);

    //分页查询出库单
    IPage<OutStore> outStorePage(Page<OutStore> outStorePage, OutStore outStore);

    //确定出库
    Result confirmOutStore(OutStore outStore);
}
