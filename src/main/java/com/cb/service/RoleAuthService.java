package com.cb.service;

import com.cb.entity.RoleAuth;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 *
 */
public interface RoleAuthService extends IService<RoleAuth> {

    //根据角色id查询角色权限表
    List<RoleAuth> getByUserId(Integer roleId);

    //根据角色id删除给角色已分配的所有权限(菜单)
    int delAuthByRoleId(Integer roleId);
}
