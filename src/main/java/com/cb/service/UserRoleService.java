package com.cb.service;

import com.cb.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface UserRoleService extends IService<UserRole> {
    //根据用户id查询用户角色表
    List<UserRole> getByUserId(Integer userId);

    //根据用户id删除给用户已分配的所有角色
    int delRoleByUserId(Integer userId);
}
