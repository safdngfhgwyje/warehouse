package com.cb.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cb.entity.User;
import com.cb.utils.Result;

import java.util.List;

public interface UserService extends IService<User> {

    //根据用户名查找用户的方法
    User findUserByCode(String userCode);

    //分页查询用户
    IPage<User> getUser(Page<User> pageInfo, User user);

    //根据create_by查询名字
    User getNameByCreateId(int createBy);

    //添加用户
    Result addUser(User user);

    //删除用户
    void deleteUser(Integer userId);

    //批量删除
    int deleteUserList(List<Integer> userIds);

    //修改用户
    Result updateUserName(User user);

    //重置密码
    Result resetPassWord(Integer userId);
}
