package com.cb.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cb.entity.InStore;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cb.utils.Result;

public interface InStoreService extends IService<InStore> {

    //添加入库单
    Result saveInStore(InStore inStore, Integer buyId);

    //分页查询入库单
    IPage<InStore> queryInStorePage(Page<InStore> inStorePage, InStore inStore);

    //确定入库
    Result confirmInStore(InStore inStore);
}
