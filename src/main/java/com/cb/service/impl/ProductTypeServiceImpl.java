package com.cb.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cb.entity.ProductType;
import com.cb.service.ProductTypeService;
import com.cb.mapper.ProductTypeMapper;
import com.cb.utils.Result;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@CacheConfig(cacheNames = "com.cb.service.impl.ProductTypeServiceImpl")
@Service
public class ProductTypeServiceImpl extends ServiceImpl<ProductTypeMapper, ProductType> implements ProductTypeService{

    @Cacheable(key = "'all:typeTree'")
    @Override
    public List<ProductType> allProductTypeTree() {
        //查询所有商品分类
        List<ProductType> allproductType = list();
        List<ProductType> typeList = allTypeToTypeTree(allproductType, 0);
        return typeList;
    }

    @Override
    public Result queryTypeByCode(String typeCode) {
        LambdaQueryWrapper<ProductType> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ProductType::getTypeCode,typeCode);
        ProductType productType = getOne(queryWrapper);
        return Result.ok(productType==null);
    }

    @CacheEvict(key = "'all:typeTree'")
    @Override
    public Result saveProductType(ProductType productType) {
        int result = baseMapper.insert(productType);
        if(result>0){
            return Result.ok("分类添加成功！");
        }else {
            return Result.err(Result.CODE_ERR_BUSINESS, "分类添加失败！");
        }
    }

    @CacheEvict(key = "'all:typeTree'")
    @Override
    public Result removeProductType(Integer typeId) {
        LambdaQueryWrapper<ProductType> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ProductType::getParentId,typeId).or().eq(ProductType::getTypeId,typeId);
        boolean remove = remove(queryWrapper);
        if(remove){
            return Result.ok("分类删除成功！");
        }
        return Result.err(Result.CODE_ERR_BUSINESS, "分类删除失败！");
    }

    @CacheEvict(key = "'all:typeTree'")
    @Override
    public Result updateProductType(ProductType productType) {
        boolean result = updateById(productType);
        if(result){
            return Result.ok("分类修改成功！");
        }
        return Result.err(Result.CODE_ERR_BUSINESS, "分类修改失败！");
    }

    //将查询到的所有商品分类List<ProductType>转成商品分类树List<ProductType>的算法
    private List<ProductType> allTypeToTypeTree(List<ProductType> typeList, Integer pid){
        List<ProductType> firstLevelType=new ArrayList<>();
        for (ProductType productType : typeList) {
            if (productType.getParentId().equals(pid)) {
                firstLevelType.add(productType);
            }
        }
        for (ProductType productType : firstLevelType) {
            List<ProductType> secondLevelType=allTypeToTypeTree(typeList,productType.getTypeId());
            productType.setChildProductCategory(secondLevelType);
        }
        return firstLevelType;
    }
}