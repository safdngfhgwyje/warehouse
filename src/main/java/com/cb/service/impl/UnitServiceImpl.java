package com.cb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cb.entity.Unit;
import com.cb.service.UnitService;
import com.cb.mapper.UnitMapper;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@CacheConfig(cacheNames = "com.cb.service.impl.UnitServiceImpl")
@Service
public class UnitServiceImpl extends ServiceImpl<UnitMapper, Unit> implements UnitService{

    @Cacheable(key = "'all:unit'")
    @Override
    public List<Unit> queryAllUnit() {
        return list();
    }
}




