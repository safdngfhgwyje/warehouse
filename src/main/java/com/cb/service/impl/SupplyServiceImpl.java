package com.cb.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cb.entity.Supply;
import com.cb.service.SupplyService;
import com.cb.mapper.SupplyMapper;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@CacheConfig(cacheNames = "com.cb.service.impl.SupplyServiceImpl")
@Service
public class SupplyServiceImpl extends ServiceImpl<SupplyMapper, Supply> implements SupplyService{

    @Cacheable(key = "'all:supply'")
    @Override
    public List<Supply> queryAllSupply() {
        LambdaQueryWrapper<Supply> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Supply::getIsDelete,0);
        return list(queryWrapper);
    }
}




