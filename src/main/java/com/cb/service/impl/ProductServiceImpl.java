package com.cb.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cb.entity.Product;
import com.cb.mapper.BrandMapper;
import com.cb.service.ProductService;
import com.cb.mapper.ProductMapper;
import com.cb.utils.GiteeImgBedUtils;
import com.cb.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements ProductService{

    @Autowired
    private BrandMapper brandMapper;

    @Override
    public IPage<Product> queryProductPage(Page<Product> productPage, Product product) {
        return baseMapper.selectProductPage(productPage,product);
    }

    @Override
    public Result saveProduct(Product product) {
        product.setCreateTime(new Date());
        product.setUpDownState("0");
        int result = baseMapper.insert(product);
        if(result>0){
            return Result.ok("添加商品成功！");
        }else {
            return Result.err(Result.CODE_ERR_BUSINESS, "添加商品失败！");
        }
    }

    @Override
    public Result updateProductState(Product product) {
        int result = baseMapper.updateById(product);
        if(result>0){
            return Result.ok("修改成功！");
        }else {
            return Result.err(Result.CODE_ERR_BUSINESS, "修改失败！");
        }
    }

    @Override
    public Result deleteById(Integer productId) {
        LambdaQueryWrapper<Product> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Product::getProductId,productId);
        Product product = getOne(queryWrapper);
        int result1 = GiteeImgBedUtils.deleteByName(product.getImgs());
        boolean result = removeById(productId);
        if(result&&result1==1){
            return Result.ok("商品删除成功！");
        }else {
            return Result.err(Result.CODE_ERR_BUSINESS, "商品删除失败！");
        }
    }

    @Override
    public Result deleteBatch(List<Integer> ids) {
        LambdaQueryWrapper<Product> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(Product::getProductId,ids);
        List<Product> productList = list(queryWrapper);
        int cnt=productList.size();
        for (Product product : productList) {
            GiteeImgBedUtils.deleteByName(product.getImgs());
            cnt--;
        }
        boolean result = removeBatchByIds(ids);
        if(result&&cnt==0){
            return Result.ok("商品批量删除成功！");
        }else {
            return Result.err(Result.CODE_ERR_BUSINESS, "商品批量删除失败！");
        }
    }

    @Override
    public Result updateProduct(Product product) {
        product.setUpdateTime(new Date());
        int result = baseMapper.updateById(product);
        if(result>0){
            return Result.ok("商品修改成功！");
        }else {
            return Result.err(Result.CODE_ERR_BUSINESS,"商品修改失败！");
        }
    }
}




