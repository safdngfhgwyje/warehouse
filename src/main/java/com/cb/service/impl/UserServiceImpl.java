package com.cb.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cb.entity.User;
import com.cb.mapper.UserMapper;
import com.cb.service.UserService;
import com.cb.utils.DigestUtil;
import com.cb.utils.Result;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Override
    public User findUserByCode(String userCode) {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getIsDelete,0);
        queryWrapper.eq(User::getUserCode,userCode);
        return getOne(queryWrapper);
    }

    @Override
    public IPage<User> getUser(Page<User> pageInfo, User user) {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        String userCode = user.getUserCode();
        String userType = user.getUserType();
        String userState = user.getUserState();
        queryWrapper.like(!StringUtils.isEmpty(userCode),User::getUserCode,userCode);
        queryWrapper.eq(!StringUtils.isEmpty(userType),User::getUserType,userType);
        queryWrapper.eq(!StringUtils.isEmpty(userState),User::getUserState,userState);
        queryWrapper.eq(User::getIsDelete,0);
        queryWrapper.orderByDesc(User::getCreateTime);
        return baseMapper.selectPage(pageInfo, queryWrapper);
    }

    @Override
    public User getNameByCreateId(int createBy) {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getUserId,createBy);
        return getOne(queryWrapper);
    }

    @Override
    public Result addUser(User user) {
        //根据用户名查询用户
        User user1 = findUserByCode(user.getUserCode());
        if (user1!=null) {
            return Result.err(Result.CODE_ERR_BUSINESS,"该用户已存在！");
        }
        //用户不存在,对密码加密,添加用户
        String pwd = DigestUtil.hmacSign(user.getUserPwd());
        user.setUserPwd(pwd);
        user.setUserState("0");
        user.setIsDelete("0");
        user.setCreateTime(new Date());
        save(user);
        return Result.ok("添加用户成功！");
    }

    @Override
    public void deleteUser(Integer userId) {
        //设置is_delete字段为1
        LambdaUpdateWrapper<User> queryWrapper = new LambdaUpdateWrapper<>();
        queryWrapper.eq(User::getUserId,userId).set(User::getIsDelete,1);
        baseMapper.update(null,queryWrapper);
    }

    @Override
    public int deleteUserList(List<Integer> userIds) {
        LambdaUpdateWrapper<User> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.in(User::getUserId,userIds).set(User::getIsDelete,1);
        return baseMapper.update(null,updateWrapper);
    }

    @Override
    public Result updateUserName(User user) {
        LambdaUpdateWrapper<User> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(User::getUserId,user.getUserId());
        user.setUpdateTime(new Date());
        boolean result = update(user, updateWrapper);
        if(result) {
            return Result.ok("用户修改成功！");
        }else {
            return Result.err(Result.CODE_ERR_BUSINESS, "用户修改失败！");
        }
    }

    @Override
    public Result resetPassWord(Integer userId) {
        LambdaUpdateWrapper<User> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(User::getUserId,userId);
        //创建User对象并保存用户id和加密后的重置密码123456
        User user = new User();
        user.setUserId(userId);
        user.setUserPwd(DigestUtil.hmacSign("12345"));
        boolean result = update(user, updateWrapper);
        if(result) {
            return Result.ok("密码重置成功！");
        }else {
            return Result.err(Result.CODE_ERR_BUSINESS, "密码重置失败！");
        }
    }
}
