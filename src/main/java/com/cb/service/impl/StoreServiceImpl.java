package com.cb.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cb.entity.Statistics;
import com.cb.entity.Store;
import com.cb.service.StoreService;
import com.cb.mapper.StoreMapper;
import com.cb.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@CacheConfig(cacheNames = "com.cb.service.impl.StoreServiceImpl")
@Service
public class StoreServiceImpl extends ServiceImpl<StoreMapper, Store> implements StoreService{

    @Autowired
    private StoreMapper storeMapper;

    @Cacheable(key = "'all:store'")
    @Override
    public List<Store> queryAllStore() {
        return list();
    }

    @Override
    public List<Statistics> statisticsStoreInvent() {
        return storeMapper.statisticsStoreInvent();
    }

    @Override
    public IPage<Store> queryStorePage(Page<Store> storePage, Store store) {
        LambdaQueryWrapper<Store> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(Store::getStoreName,store.getStoreName())
                .like(Store::getStoreAddress,store.getStoreAddress())
                .like(Store::getConcat,store.getConcat())
                .like(Store::getPhone,store.getPhone());
        return baseMapper.selectPage(storePage,queryWrapper);
    }

    @Override
    public Result checkStoreNum(String storeNum) {
        LambdaQueryWrapper<Store> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Store::getStoreNum,storeNum);
        Store one = getOne(queryWrapper);
        return Result.ok(one==null);
    }
}




