package com.cb.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cb.entity.BuyList;
import com.cb.service.BuyListService;
import com.cb.mapper.BuyListMapper;
import com.cb.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class BuyListServiceImpl extends ServiceImpl<BuyListMapper, BuyList> implements BuyListService{

    @Autowired
    private BuyListMapper buyListMapper;

    @Override
    public Result savePurchase(BuyList buyList) {
        buyList.setBuyTime(new Date());
        buyList.setIsIn("0");
        int insert = baseMapper.insert(buyList);
        if (insert > 0) {
            return Result.ok("采购单添加成功！");
        }else {
            return Result.err(Result.CODE_ERR_BUSINESS, "采购单添加失败！");
        }
    }

    @Override
    public IPage<BuyList> queryPurchasePage(Page<BuyList> buyListPage, BuyList buyList) {
        return buyListMapper.selectPurchasePage(buyListPage, buyList);
    }

    @Override
    public Result deletePurchase(Integer buyId) {
        int result = baseMapper.deleteById(buyId);
        if(result>0){
            return Result.ok("采购单删除成功！");
        }
        return Result.err(Result.CODE_ERR_BUSINESS, "采购单删除失败！");
    }

    @Override
    public Result updatePurchase(BuyList purchase) {
        LambdaUpdateWrapper<BuyList> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(BuyList::getBuyNum,purchase.getBuyNum())
                .set(BuyList::getFactBuyNum,purchase.getFactBuyNum())
                .eq(BuyList::getBuyId,purchase.getBuyId());
        int result = baseMapper.update(null, updateWrapper);
        if(result>0){
            return Result.ok("采购单修改成功！");
        }
        return Result.err(Result.CODE_ERR_BUSINESS, "采购单修改失败！");
    }
}




