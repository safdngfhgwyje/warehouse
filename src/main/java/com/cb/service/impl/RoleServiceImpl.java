package com.cb.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cb.dto.AssignRoleDto;
import com.cb.entity.Auth;
import com.cb.entity.Role;
import com.cb.entity.RoleAuth;
import com.cb.entity.UserRole;
import com.cb.mapper.RoleMapper;
import com.cb.service.RoleAuthService;
import com.cb.service.RoleService;
import com.cb.service.UserRoleService;
import com.cb.utils.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {
    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    @Lazy
    private RoleService roleService;

    @Autowired
    private RoleAuthService roleAuthService;


    @Override
    public List<Role> queryAllRole() {
        LambdaQueryWrapper<Role> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Role::getRoleState,1);
        return list(queryWrapper);
    }

    @Override
    public List<Role> queryRolesByUserId(Integer userId) {
        List<Role> roles = new ArrayList<>();
        List<UserRole> userRoles = userRoleService.getByUserId(userId);
        if(userRoles.size()==0){
            return roles;
        }
        ArrayList<Integer> integers = new ArrayList<>();
        for (UserRole role : userRoles) {
            integers.add(role.getRoleId());
        }
        LambdaQueryWrapper<Role> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(Role::getRoleId,integers);
        roles=baseMapper.selectList(queryWrapper);
        return roles;
    }

    @Override
    public int getRoleIdByName(String roleName) {
        LambdaQueryWrapper<Role> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Role::getRoleName,roleName);
        Role role = getOne(queryWrapper);
        return role.getRoleId();
    }

    @Override
    public void assign(AssignRoleDto assignRoleDto) {
        //用户id
        Integer userId = assignRoleDto.getUserId();
        //用户分配的所有角色名
        List<String> roleCheckList = assignRoleDto.getRoleCheckList();

        //根据用户id删除给用户已分配的所有角色
        userRoleService.delRoleByUserId(assignRoleDto.getUserId());
        for (String roleName : roleCheckList) {
            //根据角色名称查询角色id
            int roleId = roleService.getRoleIdByName(roleName);
            //添加用户角色关系的方法
            UserRole userRole = new UserRole();
            userRole.setRoleId(roleId);
            userRole.setUserId(userId);
            userRoleService.save(userRole);
        }
    }

    @Override
    public IPage<Role> getRole(Page<Role> rolePage, Role role) {
        LambdaQueryWrapper<Role> queryWrapper = new LambdaQueryWrapper<>();
        String roleName = role.getRoleName();
        String roleCode = role.getRoleCode();
        String roleState = role.getRoleState();
        queryWrapper.like(!StringUtils.isEmpty(roleName),Role::getRoleName,roleName);
        queryWrapper.like(!StringUtils.isEmpty(roleCode),Role::getRoleCode,roleCode);
        queryWrapper.eq(!StringUtils.isEmpty(roleState),Role::getRoleState,roleState);
        queryWrapper.orderByDesc(Role::getCreateTime);
        return baseMapper.selectPage(rolePage,queryWrapper);
    }

    @Override
    public Result saveRole(Role role) {
        LambdaQueryWrapper<Role> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Role::getRoleName,role.getRoleName()).or().eq(Role::getRoleCode,role.getRoleCode());
        Role role1 = getOne(queryWrapper);
        if (role1!=null) {
            return Result.err(Result.CODE_ERR_BUSINESS, "该角色已存在！");
        }
        //角色不存在,添加角色
        role.setRoleState("0");
        role.setCreateTime(new Date());
        baseMapper.insert(role);
        return Result.ok("添加角色成功！");
    }

    @Override
    public void deleteRole(Integer roleId) {
        //根据角色id删除角色
        boolean result = removeById(roleId);
        if(result){
            //根据角色id删除给角色已分配的所有权限(菜单)
            roleAuthService.delAuthByRoleId(roleId);
        }
    }

    @Override
    public Result updateRole(Role role) {
        LambdaUpdateWrapper<Role> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(Role::getRoleId,role.getRoleId());
        role.setUpdateTime(new Date());
        boolean result = update(role, updateWrapper);
        if(result){
            return Result.ok("角色修改成功！");
        }else {
            return Result.err(Result.CODE_ERR_BUSINESS, "角色修改失败！");
        }
    }
}
