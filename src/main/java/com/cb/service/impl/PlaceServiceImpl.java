package com.cb.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cb.entity.Place;
import com.cb.service.PlaceService;
import com.cb.mapper.PlaceMapper;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@CacheConfig(cacheNames = "com.cb.service.impl.PlaceServiceImpl")
@Service
public class PlaceServiceImpl extends ServiceImpl<PlaceMapper, Place> implements PlaceService{

    @Cacheable(key = "'all:place'")
    @Override
    public List<Place> queryAllPlace() {
        LambdaQueryWrapper<Place> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Place::getIsDelete,0);
        return list(queryWrapper);
    }
}




