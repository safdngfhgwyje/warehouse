package com.cb.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cb.entity.RoleAuth;
import com.cb.service.RoleAuthService;
import com.cb.mapper.RoleAuthMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 */
@Service
public class RoleAuthServiceImpl extends ServiceImpl<RoleAuthMapper, RoleAuth> implements RoleAuthService{

    @Override
    public List<RoleAuth> getByUserId(Integer roleId) {
        LambdaQueryWrapper<RoleAuth> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(RoleAuth::getRoleId,roleId);
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public int delAuthByRoleId(Integer roleId) {
        LambdaQueryWrapper<RoleAuth> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(RoleAuth::getRoleId,roleId);
        return remove(queryWrapper)?1:0;
    }
}




