package com.cb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cb.entity.Brand;
import com.cb.service.BrandService;
import com.cb.mapper.BrandMapper;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@CacheConfig(cacheNames = "com.cb.service.impl.BrandServiceImpl")
@Service
public class BrandServiceImpl extends ServiceImpl<BrandMapper, Brand> implements BrandService{

    @Cacheable(key = "'all:brand'")
    @Override
    public List<Brand> queryAllBrand() {
        return list();
    }
}




