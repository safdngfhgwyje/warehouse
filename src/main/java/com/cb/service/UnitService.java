package com.cb.service;

import com.cb.entity.Unit;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface UnitService extends IService<Unit> {

    //查询所有单位
    List<Unit> queryAllUnit();
}
