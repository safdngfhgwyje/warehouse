package com.cb.service;

import com.cb.entity.Brand;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface BrandService extends IService<Brand> {

    //查询所有品牌
    List<Brand> queryAllBrand();
}
