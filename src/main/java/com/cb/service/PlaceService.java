package com.cb.service;

import com.cb.entity.Place;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface PlaceService extends IService<Place> {

    //查询所有产地
    List<Place> queryAllPlace();
}
