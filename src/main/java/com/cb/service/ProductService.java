package com.cb.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cb.entity.Product;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cb.utils.Result;

import java.util.List;

public interface ProductService extends IService<Product> {

    //分页查询商品
    IPage<Product> queryProductPage(Page<Product> productPage, Product product);

    //添加商品
    Result saveProduct(Product product);

    //修改商品上下架状态
    Result updateProductState(Product product);

    //删除商品
    Result deleteById(Integer productId);

    //批量删除商品
    Result deleteBatch(List<Integer> ids);

    //修改商品
    Result updateProduct(Product product);
}
