package com.cb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cb.dto.AssignAuthDto;
import com.cb.entity.Auth;

import java.util.List;

public interface AuthService extends IService<Auth> {
    //根据用户id查询用户权限(菜单)树的业务方法
    List<Auth> findAuthTree(int userId);

    //查询整个权限(菜单)树的业务方法
    List<Auth> allAuthTree();

    //查询角色已分配的权限
    List<Auth> queryRoleAuth(Integer roleId);

    //给角色分配权限
    void assignAuth(AssignAuthDto assignAuthDto);
}
