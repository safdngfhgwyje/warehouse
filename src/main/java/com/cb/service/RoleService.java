package com.cb.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cb.dto.AssignRoleDto;
import com.cb.entity.Auth;
import com.cb.entity.Role;
import com.cb.utils.Result;

import java.util.List;

public interface RoleService extends IService<Role> {

    //查询所有角色
    List<Role> queryAllRole();

    //查询用户已分配的角色
    List<Role> queryRolesByUserId(Integer userId);

    //根据角色名称查询角色id
    int getRoleIdByName(String roleName);

    //给用户分配角色
    void assign(AssignRoleDto assignRoleDto);

    //分页查询角色
    IPage<Role> getRole(Page<Role> rolePage, Role role);

    //添加角色
    Result saveRole(Role role);

    //删除角色
    void deleteRole(Integer roleId);

    //修改角色
    Result updateRole(Role role);
}
