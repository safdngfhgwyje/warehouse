package com.cb.service;

import com.cb.entity.Supply;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface SupplyService extends IService<Supply> {

    //查询所有供应商
    List<Supply> queryAllSupply();
}
