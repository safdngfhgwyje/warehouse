package com.cb.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cb.entity.Statistics;
import com.cb.entity.Store;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cb.utils.Result;

import java.util.List;

public interface StoreService extends IService<Store> {

    //查询所有仓库
    List<Store> queryAllStore();

    //统计各个仓库商品库存数量的方法
    List<Statistics> statisticsStoreInvent();

    //分页查询仓库
    IPage<Store> queryStorePage(Page<Store> storePage, Store store);

    //校验仓库编号
    Result checkStoreNum(String storeNum);
}
