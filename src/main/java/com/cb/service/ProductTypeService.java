package com.cb.service;

import com.cb.entity.ProductType;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cb.utils.Result;

import java.util.List;

public interface ProductTypeService extends IService<ProductType> {

    //查询所有商品分类树
    List<ProductType> allProductTypeTree();

    //校验分类编码是否已存在
    Result queryTypeByCode(String typeCode);

    //添加商品分类
    Result saveProductType(ProductType productType);

    //删除商品分类
    Result removeProductType(Integer typeId);

    //修改商品分类
    Result updateProductType(ProductType productType);
}
